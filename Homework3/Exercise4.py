import numpy as np

a = np.random.uniform(-10, 10, size=(10, 8))
print(a)
num = float(input("Enter number: "))
print("The closest value: ", a.flatten()[np.abs(a-num).argmin()])
print("The 3 closest values: ", ", ".join([str(val) for val in a.flatten()[np.argsort(np.abs(a-num), axis=None)[:3]]]))

# using numpy argpartition (without sorting --> less time consumption)
print("The 3 closest values: ", ", ".join([str(val) for val in a.flatten()[np.argpartition(np.abs(a-num), 3, axis=None)[:3]]]))
