import numpy as np

m, n, k = map(int, input("Enter M, N, K: ").split(','))
a = np.random.random_integers(-100, 100, size=(m, n, k))
print(a, end="\n\n")

print("Find max, min, sum by depth: \n")
print(a.max(axis=0), end="\n\n")
print(a.min(axis=0), end="\n\n")
print(a.sum(axis=0), end="\n\n")

print("Find max, min, sum by height: \n")
print(a.max(axis=1), end="\n\n")
print(a.min(axis=1), end="\n\n")
print(a.sum(axis=1), end="\n\n")

print("Find max, min, sum by width: \n")
print(a.max(axis=2), end="\n\n")
print(a.min(axis=2), end="\n\n")
print(a.sum(axis=2))
