def define_range(start, end, step):
    r = start + step
    while r < end:
        yield r
        r += step


list_numbers = list(x ** 2 / (4 * x) for x in define_range(0, 10, 0.1))
print(list_numbers)
print(len(list_numbers))