input_numbers = input("Enter numbers: ")
numbers = input_numbers.split(",")
print("Even numbers: ", ", ".join([x for x in numbers if int(x) % 2 == 0]))
print("Odd numbers: ", ", ".join([x for x in numbers if int(x) % 2 != 0]))