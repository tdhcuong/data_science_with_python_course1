def calculate_key_sum(elements):
    key_sum = 0
    for element in elements:
        if isinstance(element, int) or isinstance(element, float):
            key_sum += element
    return key_sum


l = [(1, 4, 'ewe', '5'), ('21', 0.4, 4, [31, 3, 5]), [7, 3, 's', 2], [4, 2, 6, 'dad'], {3, 5}]
l.sort(key=calculate_key_sum)
print(l)
