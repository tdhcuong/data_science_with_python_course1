import random

n = input("Enter number: ")
list_numbers = [random.randint(0, 10) for _ in range(int(n))]
print(list_numbers)

# print a histogram
print("Histogram: \n")
for number in list_numbers:
    print(int(number) * "*")
print()

# statistics description of number list
print("Max: ", max(list_numbers))
print("Min: ", min(list_numbers))

# manually calculate max
# max = list_numbers[0];
# for n in list_numbers:
#     if n > max:
#        max = n
# print(max)

# manually calculate min
# min = list_numbers[0];
# for n in list_numbers:
#     if n < min:
#        min = n
# print(min)

# calculate mean
mean = sum(list_numbers)/float(len(list_numbers))
print("Mean: ", mean)

# calculate variance
print("Variance: ", sum((x-mean) ** 2 for x in list_numbers)/float(len(list_numbers)-1))

# Calculate standard deviation
print("Standard deviation: ", (sum((x-mean) ** 2 for x in list_numbers)/float(len(list_numbers))) ** (1/2))

# remove duplicate
print("Remove duplicate numbers: ", list(set(list_numbers)))