import pandas as pd

df = pd.read_csv("iris.txt", names=['sepal_length', 'sepal_width', 'petal_length', 'petal_width', 'species'])
#df = pd.read_csv("Iris_Data.csv")

df_group = df.groupby('species')
print(df_group.describe().to_string())
print(df_group[['sepal_length', 'petal_length']].transform(lambda x: (x - x.mean())/2))