import pandas as pd

str = "Ah meta descriptions… the last bastion of traditional marketing! The only cross-over point between marketing " \
      "and search engine optimisation! The knife edge between beautiful branding and an online suicide note!"

s = pd.Series(str.split(" "))
mask = s.map(lambda x: sum(map(x.lower().count, "aeiou")) >= 3)
print(s[mask])